import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import DatePicker from 'react-native-datepicker';
import { Content, Form, Picker, Item, Icon, Button } from 'native-base';
import { Input, Spinner } from './common';

export default class Signup extends React.Component {
  static navigationOptions = {
    title: 'Registration'
  };

  state = { firstname:'',
        lastname:'',
        gender:'Male',
        username:'',
        password:'',
        isLoading: false}

  buttonListener() {
    const { firstname,lastname,gender,username,password } = this.state
    alert(firstname+", "+lastname+", "+gender+", "+username+", "+password)
  }
  genderChange(value: string) {
    this.setState({
      gender: value
    })
  }
  renderButton() {
    if (this.state.isLoading) {
      return <Spinner size='large' />;
      }
      return (
        <Button
          style={{justifyContent:'center',backgroundColor:'green',borderRadius:10}}
          onPress={() => this.postRequest()}>
          <Text style={{fontSize:18,color:'#fff'}}>REGISTER</Text>
        </Button>
      )
  }
  postRequest() {
    const { isLoading } = this.state
    this.setState({isLoading: true})
    fetch('http://192.168.62.2:3005/insert', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json;charset=UTF-8'
        },
        body: JSON.stringify({
          firstname: this.state.firstname,
          lastname: this.state.lastname,
          gender: this.state.gender,
          username: this.state.username,
          password: this.state.password
        })
    })
    .then((response) => {
      response.json()
      .then((data) => {
        if (data.message === 'Successfully inserted user') {
          alert('Successfully insert user')
          this.setState({isLoading: false})
        } else {
          alert('Cannot insert user!')
        }
      })
    })
    .catch((error) => {
      alert('You did me wrong!'+error)
    })
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.cardcontainerStyle}>
          <Text style={{marginLeft:20,fontWeight:'bold',marginTop:10}}>First Name</Text>
          <View style={styles.cardsectioncontainerStyle}>
            <Input
              placeholder='Enter firstname'
              value={this.state.firstname}
              onChangeText={firstname => this.setState({ firstname })}
            />
          </View>
          <Text style={{marginLeft:20,fontWeight:'bold'}}>Last Name</Text>
          <View style={styles.cardsectioncontainerStyle}>
            <Input
              placeholder='Enter lastname'
              value={this.state.lastname}
              onChangeText={lastname => this.setState({ lastname })}
            />
          </View>
          <Text style={{marginLeft:20,fontWeight:'bold'}}>Gender</Text>
            <View style={styles.cardsectioncontainerStyle}>
              <Content>
                <Form>
                  <Item picker>
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      style={{ marginLeft:5,marginRight:5 }}
                      placeholderStyle={{ color: "#bfc6ea" }}
                      placeholderIconColor="#007aff"
                      selectedValue={this.state.gender}
                      onValueChange={this.genderChange.bind(this)}
                    >
                      <Picker.Item label="Male" value="Male"/>
                      <Picker.Item label="Female" value="Female"/>
                    </Picker>
                  </Item>
                </Form>
              </Content>
            </View>
          <Text style={{marginLeft:20,fontWeight:'bold'}}>Username</Text>
          <View style={styles.cardsectioncontainerStyle}>
            <Input
              placeholder='Enter email address'
              value={this.state.username}
              onChangeText={username => this.setState({ username })}
            />
          </View>
          <Text style={{marginLeft:20,fontWeight:'bold'}}>Password</Text>
          <View style={styles.cardsectioncontainerStyle}>
            <Input
              secureTextEntry
              placeholder='Enter password'
              value={this.state.password}
              onChangeText={password => this.setState({ password })}
            />
          </View>
          <View style={{
            padding: 5,
            backgroundColor: '#fff',
            justifyContent: 'center',
            position: 'relative',
            marginBottom: 12,
            marginTop: 12,
            marginLeft:20,
            marginRight:20 }}>
            {this.renderButton()}
          </View>
        </View>
      </ScrollView>
    )
  }
}


const styles = {
  cardcontainerStyle: {
    flex:1,
    elevation: 1,
    borderRadius: 10,
    borderColor: '#A1A3A1',
    borderWidth: 1,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    backgroundColor: 'white'
  },
  cardsectioncontainerStyle: {
    padding: 5,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative',
    borderRadius: 10,
    borderColor: '#A1A3A1',
    borderWidth: 1,
    marginTop: 10,
    marginBottom: 5,
    marginLeft: 20,
    marginRight: 20
  },
  inputStyle: {
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    position: 'relative'
  }
}
