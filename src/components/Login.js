import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Button } from 'native-base'
import { Input, Spinner } from './common';

export default class Login extends React.Component {

  state = { email: '', password: '', error: '', loading: false }

  onButtonPress() {
    const { email, password } = this.state;
    this.setState({ loading: true });
    { this.postRequest() }
  }
  onLoginFail() {
    this.setState({
      error: 'Authentication Failed!',
      loading: false
    });
    alert('Fail to login!')
  }
  onLoginSuccess() {
    this.setState({
      email: '',
      password: '',
      loading: false,
      error: ''
    });
    this.props.navigation.navigate('AppDrawerNavigator')
  }

  renderButton() {
    if (this.state.loading) {
      return <Spinner size='large' />;
    }
    return (
      <Button
        style={{ justifyContent: 'center', backgroundColor: 'green', borderRadius: 10 }}
        onPress={this.onButtonPress.bind(this)}>
        <Text style={{ fontSize: 22, color: '#fff' }}>LOGIN</Text>
      </Button>
    )
  }
  getRequest() {
    fetch('http://192.168.100.182:3005/', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'
      }
    })
      .then((response) => {
        response.json()
          .then((data) => {
            alert(JSON.stringify(data))
          })
      })
      .catch((error) => {
        alert('You did me wrong!' + error)
      })
  }
  postRequest() {
    fetch('http://192.168.100.182:3005/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'
      },
      body: JSON.stringify({
        username: this.state.email,
        password: this.state.password
      })
    })
      .then((response) => {
        response.json()
          .then((data) => {
            if (data.message === 'ok') {
              { this.onLoginSuccess() }
            } else {
              { this.onLoginFail() }
            }
          })
      })
      .catch((error) => {
        this.setState({ loading: false })
        alert('You did me wrong!' + error)
      })
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.cardcontainerStyle}>
        <View style={styles.logoStyle}>
          <Image
            source={require('../../assets/images/intellicare-logo.png')}
            style={styles.logoImageStyle}
          />
        </View>
        <View style={styles.cardsectioncontainerStyle}>
          <Image
            source={require('../../assets/icons/user.png')}
            style={styles.iconStyle}
          />
          <Input
            placeholder='user@gmail.com'
            value={this.state.email}
            onChangeText={email => this.setState({ email })}
          />
        </View>
        <View style={styles.cardsectioncontainerStyle}>
          <Image
            source={require('../../assets/icons/password.png')}
            style={styles.iconStyle}
          />
          <Input
            secureTextEntry
            placeholder='password'
            value={this.state.password}
            onChangeText={password => this.setState({ password })}
          />
        </View>
        <TouchableOpacity style={{
          alignSelf: 'flex-end',
          marginTop: 10, marginRight: 40
        }}
          onPress={() => navigate('ForgotPasswordForm')}>
          <Text style={{ textDecorationLine: 'underline' }}>
            Forgot password?
            </Text>
        </TouchableOpacity>
        <View style={{
          padding: 5,
          backgroundColor: '#fff',
          justifyContent: 'center',
          marginTop: 12,
          marginLeft: 26,
          marginRight: 26
        }}>
          {/* {this.renderButton()} */}
          <Button
            style={{ justifyContent: 'center', backgroundColor: 'green', borderRadius: 10 }}
            onPress={() => this.props.navigation.navigate('AppDrawerNavigator')}>
            <Text style={{ fontSize: 22, color: '#fff' }}>LOGIN</Text>
          </Button>
        </View>
        <View style={styles.footerLogoStyle}>
          <Image
            source={require('../../assets/images/Fullerton-Healthcare.png')}
            style={{ height: 20, width: 100 }}
          />
        </View>
        <Text style={{ fontSize: 10, textAlign: 'center', marginTop: 10 }}>© Copyright 2019 intellicare All Rights Reserved</Text>
      </View>
    )
  }
}

const styles = {
  cardcontainerStyle: {
    flex: 1,
    elevation: 1,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    backgroundColor: 'white'
  },
  cardsectioncontainerStyle: {
    padding: 5,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative',
    borderRadius: 10,
    borderColor: '#A1A3A1',
    borderWidth: 1,
    marginTop: 2,
    marginBottom: 2,
    marginLeft: 30,
    marginRight: 30
  },
  inputStyle: {
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    position: 'relative'
  },
  iconStyle: {
    marginLeft: 45,
    height: 35,
    width: 35
  },
  logoStyle: {
    padding: 5,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    position: 'relative',
  },
  logoImageStyle: {
    height: 60,
    width: 200,
    marginTop: 100,
    marginBottom: 50
  },
  footerLogoStyle: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    position: 'relative',
    marginTop: 90
  }
}
