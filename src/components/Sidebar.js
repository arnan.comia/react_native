import React from 'react';
import { View, Image } from "react-native";
import { Accordion, Content, Button, ListItem, Text, Left, Body, Right, Switch } from 'native-base';

export default class Sidebar extends React.Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 150, backgroundColor: '#2ECC71', alignItems: 'center', justifyContent: 'center' }}>
                    <Image source={require('../../assets/images/cat-profile.png')} style={{ height: 96, width: 96, borderRadius: 60 }} />
                </View>
                <Content>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: "#D5F5E3" }}>
                                <Image source={require('../../assets/images/home.png')} style={{ height: 20, width: 20, borderRadius: 60 }} />
                            </Button>
                        </Left>
                        <Body>
                            <Button transparent onPress={() => this.props.navigation.navigate('MainPage')}>
                                <Text>Home</Text>
                            </Button>
                        </Body>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: "#D5F5E3" }}>
                                <Image source={require('../../assets/images/acct-profile.png')} style={{ height: 20, width: 20, borderRadius: 60 }} />
                            </Button>
                        </Left>
                        <Body>
                            <Button transparent onPress={() => this.props.navigation.navigate('MainPage')}>
                                <Text>Account Profile/s</Text>
                            </Button>
                        </Body>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: "#D5F5E3" }}>
                                <Image source={require('../../assets/images/online-service.png')} style={{ height: 20, width: 20, borderRadius: 60 }} />
                            </Button>
                        </Left>
                        <Body>
                            <Button transparent onPress={() => this.props.navigation.navigate('MainPage')}>
                            
                            </Button>
                        </Body>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: "#D5F5E3" }}>
                                <Image source={require('../../assets/images/intellimap.png')} style={{ height: 20, width: 20, borderRadius: 60 }} />
                            </Button>
                        </Left>
                        <Body>
                            <Button transparent onPress={() => this.props.navigation.navigate('IntellicareMap')}>
                                <Text>IntelliMap</Text>
                            </Button>
                        </Body>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: "#D5F5E3" }}>
                                <Image source={require('../../assets/images/settings.png')} style={{ height: 20, width: 20, borderRadius: 60 }} />
                            </Button>
                        </Left>
                        <Body>
                            <Button transparent onPress={() => this.props.navigation.navigate('Settings')}>
                                <Text>Settings</Text>
                            </Button>
                        </Body>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: "red" }}>
                                <Image source={require('../../assets/images/sign-out.png')} style={{ height: 20, width: 20, borderRadius: 60 }} />
                            </Button>
                        </Left>
                        <Body>
                            <Button transparent onPress={() => this.props.navigation.navigate('Login')}>
                                <Text style={{ color: 'red' }}>Log out</Text>
                            </Button>
                        </Body>
                    </ListItem>
                </Content>
            </View>
        )
    }
}

const dataArray = [
    { title: "First Element", content: "Lorem ipsum dolor sit amet" },
    { title: "Second Element", content: "Lorem ipsum dolor sit amet" },
    { title: "Third Element", content: "Lorem ipsum dolor sit amet" }
];