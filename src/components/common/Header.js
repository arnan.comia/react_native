// Import Libraries for making a component
import React from 'react';
import { View, Text } from 'react-native';

// Make a component
const Header = (props) => {
  const { textStyle, viewStyle } = styles;

  return (
    <View style={viewStyle}>
      <Text style={textStyle}>Intellicare</Text>
    </View>
  );
};

//Make a style function for styling a component
const styles = {
  textStyle: {
    fontSize: 50
  },
  viewStyle: {
    backgroundColor: 'white',
    justifyContent: 'center', // justifyContent use to position item into vertical direction
    alignItems: 'center', // alignItems is use to position item into harizontal direction
    height: 320,
    position: 'relative'
  },
  imageStyle: {
    height: 50,
    width: 200
  }
};
// Make a component available to other parts of the app
export { Header };
