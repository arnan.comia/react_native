import React from 'react';
import { View, Text } from 'react-native';
import { Input, GreenButton } from './common';

export default class ForgotPasswordForm extends React.Component {
  static navigationOptions = {
    title: 'Reset Password'
  };
  render() {
    return (
      <View style={styles.cardcontainerStyle}>
        <Text style={{marginLeft:20,fontWeight:'bold'}}>Enter Email Address</Text>
        <View style={styles.cardsectioncontainerStyle}>
          <Input
            placeholder='Email Address'
          />
        </View>
        <View style={{
          padding: 5,
          backgroundColor: '#fff',
          justifyContent: 'flex-start',
          flexDirection: 'row',
          position: 'relative',
          marginTop: 10}}>
          <GreenButton>
            CONFIRM
          </GreenButton>
        </View>
      </View>
    )
  }
}
const styles = {
  cardcontainerStyle: {
    flex:1,
    elevation: 1,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    backgroundColor: 'white'
  },
  cardsectioncontainerStyle: {
    padding: 5,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative',
    borderRadius: 10,
    borderColor: '#A1A3A1',
    borderWidth: 1,
    marginTop: 10,
    marginBottom: 5,
    marginLeft: 20,
    marginRight: 20
  },
  inputStyle: {
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    position: 'relative'
  }
}
