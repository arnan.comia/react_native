import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Login from './Login';
import Signup from './Signup';
import OnBoardingScreen from './OnBoardingScreen';
import ForgotPasswordForm from './ForgotPasswordForm';
import AppDrawerNavigator from './AppDrawerNavigator'

const AppNavigator = createStackNavigator({
  OnBoardingScreen: { screen: OnBoardingScreen,
    navigationOptions:{
      header: null
  }},
  Login: { screen: Login
  },
  Signup: { screen: Signup
  },
  ForgotPasswordForm: { screen: ForgotPasswordForm
  },
  AppDrawerNavigator: { screen: AppDrawerNavigator,
    navigationOptions:{
      header: null
  }},
})

export default createAppContainer(AppNavigator);
