import React from 'react';
import { View, Image } from 'react-native';
import {
    Header, Left, Content, List, ListItem,
    Item, Label, Input, Text, Body, Right
} from "native-base";

export default class Profile extends React.Component {
    static navigationOptions = {
        title: 'Account Information'
    };
    render() {
        return (
            <View style={{ flex: 1 }}>
                <Content>
                    <Header>
                        <Left>
                            <Text>Account Information</Text>
                        </Left>
                        <Body></Body>
                    </Header>
                    <List>
                        <ListItem>
                            <Item fixedLabel>
                                <Label>Username</Label>
                                <Input />
                            </Item>
                        </ListItem>
                    </List>
                </Content>
            </View>
        )
    }
}
