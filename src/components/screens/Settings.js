import React from 'react';
import { View, Image, TouchableOpacity } from "react-native";
import { Header, Body, Left, Right } from 'native-base';
import { DrawerActions } from 'react-navigation-drawer';

export default class Settings extends React.Component {
  render() {
    return (
      <View style={{ flex: 1}}>
        <Header>
          <Left>
            <TouchableOpacity onPress={() => this.props.navigation.dispatch(DrawerActions.toggleDrawer())}>
              <Image
                source={require('../../../assets/images/drawer-menu.png')}
                style={{ height: 30, width: 30 }}
              />
            </TouchableOpacity>
          </Left>
          <Body></Body>
          <Right></Right>
        </Header>
      </View>
    )
  }
}
