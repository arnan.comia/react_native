import React from 'react';
import { Dimensions, View, StyleSheet, Image, PermissionsAndroid } from "react-native";

import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';

const clinic = require('../../../clinic.json')

export default class Settings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            latitude: 14.561423,
            longitude: 121.014235,
            locations: clinic,
            coords: []
        }
    }

    async componentDidMount() {

        const { granted } = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
                title: 'Intellicare Map Permission',
                message: 'Intellicare App needs access to your location ',
                buttonNeutral: 'Ask Me Later',
                buttonNegative: 'Cancel',
                buttonPositive: 'OK',
            },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log('You can use the location');
        } else {
            console.log('Location permission denied');
        }

        // Geolocation.getCurrentPosition(
        //     ({ coords: { latitude, longitude } }) => this.setState({ latitude, longitude }, this.mergedCoords),
        //     (error) => console.log('Error:', error)
        // )

        // Geolocation.getCurrentPosition(
        //     (position) => {
        //         console.log(position)
        //         this.setState({
        //             latitude: position.coords.latitude,
        //             longitude: position.coords.longitude,
        //             error: null,
        //         },
        //         this.mergedCoords());
        //     },
        //     (error) => this.setState({ error: error.message }),
        //     { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        // );

        const { locations: [sampleLocations] } = this.state

        // this.setState({
        //     desLatitude: sampleLocations.coords.latitude,
        //     desLongitude: sampleLocations.coords.longitude
        // }, this.mergedCoords)

    }
    // mergedCoords() {
    //     const { latitude, longitude, desLatitude, desLongitude } = this.state

    //     const hasStartAndEnd = latitude != null && longitude != null
    //     console.log(hasStartAndEnd)
    //     if (hasStartAndEnd) {
    //         const concatStart = `${latitude}, ${longitude}`
    //         const concatEnd = `${desLatitude}, ${desLongitude}`
    //         console.log('LAT:', concatStart)
    //         console.log('DESLAT:', concatEnd)
    //         this.getDirections(concatStart, concatEnd)

    //     }

    // }

    // async getDirections(startLoc, destinationLoc) {
    //     try {
    //         let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${startLoc}&destination=${destinationLoc}`)
    //         let respJson = await resp.json();
    //         let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
    //         let coords = points.map(point => {
    //             return {
    //                 latitude: point[0],
    //                 longitude: point[1]
    //             }
    //         })
    //         this.setState({ coords: coords })
    //         return coords
    //     } catch (error) {
    //         console.log('Error:', error)
    //     }
    // }
    renderMarker() {
        const { locations } = this.state
        return (
            <View>
                {
                    locations.map((location, idx) => {
                        const { coords: {
                            latitude, longitude
                        } } = location
                        console.log('Marker:', this.state)
                        return (
                            <Marker
                                key={idx}
                                coordinate={{ latitude, longitude }}
                                onPress={this.onMarkerPress(location)}
                            />
                        )
                    })
                }
            </View>
        )
    }

    onMarkerPress = location => () => {
        const { coords: { latitude, longitude } } = location
        this.setState({
            selectedHospital: location
        })
        console.log('Marker:', location)
    }

    render() {

        const { latitude, longitude, selectedHospital } = this.state

        return (
            <View style={styles.container}>
                <MapView
                    //showsUserLocation
                    provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                    style={styles.map}
                    initialRegion={{
                        latitude: 14.561387,
                        longitude: 121.01423,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121,
                    }}>
                    {this.renderMarker()}
                    <Image
                        source={{ uri: selectedHospital && selectedHospital.image_url }}
                        // style={{ 
                        //     flex: 1,
                        //     width: width * 0.95,
                        //     alignSelf: 'center',
                        //     height: 30,
                        //     position: 'absolute',
                        //     bottom: 10
                        //  }}
                    />
                </MapView>
            </View>

        )
    }
}

const { width, height } = Dimensions.get('screen')

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        flex: 1,
    },
});