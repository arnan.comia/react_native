import React from 'react';
import { Dimensions, TouchableOpacity, View, Text, Image } from 'react-native';
import { DrawerActions } from 'react-navigation-drawer';
import { Header, Left, Right, Body, Icon } from 'native-base';

export default class MainPage extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Header>
          <Left>
          <TouchableOpacity onPress={() => this.props.navigation.dispatch(DrawerActions.toggleDrawer())}>
            <Image 
              source={require('../../../assets/images/drawer-menu.png')} 
              style={{height:30,width:30}}
              />
          </TouchableOpacity>
          </Left>
          <Body></Body>
          <Right>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
            <Image 
              source={require('../../../assets/images/logged-in-user.png')} 
              style={{height:30,width:30}}
              />
          </TouchableOpacity>
          </Right>
        </Header>
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
          <Text style={{fontSize: 30}}>
            Intellicare Main Page
          </Text>
        </View>
      </View>
    );
  }
};

export const { width, height } = Dimensions.get('window');

const styles = {
  container: {
    flex: 1,
  }
}
