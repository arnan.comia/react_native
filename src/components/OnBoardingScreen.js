import React from 'react';
import { View, Text, Dimensions, TouchableOpacity } from 'react-native';
import SwiperFlatList from 'react-native-swiper-flatlist';
import { Button } from 'native-base';
import { Header } from './common'

export default class SamplePage extends React.Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={styles.container}>
        <View>
          <TouchableOpacity style={{
            alignSelf: 'flex-end',
            marginTop: 20, marginRight: 20}}
            onPress={() => navigate('MainPage')}>
          <Text style={{color:'#20C710',fontSize:12}}>
            SKIP
          </Text>
          </TouchableOpacity>
        </View>
        <Header />
        <View style={styles.swiperStyle}>
          <SwiperFlatList
            autoplay
            autoplayDelay={2}
            autoplayLoop
            index={0}
            showPagination
            paginationDefaultColor='green'
            paginationActiveColor='gray'

          >
            <View style={styles.child}>
              <Text style={{fontSize: 18, textAlign: 'center'}}>The Intellicare Advantage</Text>
              <Text style={{fontSize: 12, textAlign: 'center'}}>We focus on improving the numbers that track your health and drive down the cost of quality healthcare</Text>
            </View>
            <View style={styles.child}>
              <Text style={{fontSize: 18, textAlign: 'center'}}>The Intellicare Advantage</Text>
              <Text style={{fontSize: 12, textAlign: 'center'}}>We focus on improving the numbers that track your health and drive down the cost of quality healthcare</Text>
            </View>
            <View style={styles.child}>
              <Text style={{fontSize: 18, textAlign: 'center'}}>The Intellicare Advantage</Text>
              <Text style={{fontSize: 12, textAlign: 'center'}}>We focus on improving the numbers that track your health and drive down the cost of quality healthcare</Text>
            </View>
            <View style={styles.child}>
              <Text style={{fontSize: 18, textAlign: 'center'}}>The Intellicare Advantage</Text>
              <Text style={{fontSize: 12, textAlign: 'center'}}>We focus on improving the numbers that track your health and drive down the cost of quality healthcare</Text>
            </View>
          </SwiperFlatList>
        </View>
        <View style={styles.buttoncontainerStyle}>
          <View style={{flex:1,margin:5}}>
            <Button
              style={{justifyContent:'center',backgroundColor:'green'}}
              onPress={() => navigate('Login')}>
              <Text style={{fontSize:18,color:'#fff'}}>LOGIN</Text>
            </Button>
          </View>
          <View style={{flex:1,margin:5}}>
            <Button
              style={{justifyContent: 'center',fontSize:18}}
              bordered success
              onPress={() => navigate('Signup')}>
              <Text style={{fontSize:18,color:'#20C710'}}>SIGN UP</Text>
            </Button>
          </View>
        </View>
      </View>
    )
  }
}

export const { width, height } = Dimensions.get('window');

const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
    position: 'relative',
    backgroundColor: 'white'
  },
  child: {
    height: 150,
    width,
    justifyContent: 'center'
  },
  text: {
    fontSize: width * 0.5,
    textAlign: 'center'
  },
  buttoncontainerStyle: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    position: 'relative',
    marginBottom: 60,
    padding: 10
  },
  swiperStyle: {
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginBottom: 2,
    height: 150
  }
}
