import React from "react";
import { Dimensions } from 'react-native';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createAppContainer } from 'react-navigation';
import MainPage from './screens/MainPage';
import Settings from './screens/Settings';
import IntellicareMap from './screens/IntellicareMap';
import Profile from './screens/Profile';
import Sidebar from './Sidebar';


const AppDrawerNavigator = createDrawerNavigator({
    MainPage: {
        screen: MainPage,
    },
    Settings: {
      screen: Settings,
      navigationOptions: ({ navigation }) => ({
        title: 'Settings Page',
        headerLeft: <MainPage navigationProps={navigation} />,
        headerStyle: {
          backgroundColor: '#FF9800'
        },
        headerTintColor: '#fff',
      })
    },
    IntellicareMap: {
      screen: IntellicareMap
    },
    Profile: {
      screen: Profile
    }
}, {
    initialRouteName: 'MainPage',
    contentComponent: Sidebar,
    drawerWidth: Dimensions.get('window').width - 130,
})

export default createAppContainer(AppDrawerNavigator)
