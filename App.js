import React from 'react';
import AppNavigator from './src/components/AppNavigator';
import AppDrawerNavigator from './src/components/AppDrawerNavigator';

const App = () => {
    return (
      <AppNavigator />
    )
}
export default App;

console.disableYellowBox = true;
